package com.desafio.springboot.app.autentificacion.desafioservicioautentificacion;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class DesafioServicioAutentificacionApplicationTests {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void contextLoads() {
	}

	@Test
	public void testAutentificacion() throws Exception {
		//Probando inicio de sesion con credenciales
		this.mockMvc.perform(post("/login")
		//consulta como json: content("{\"username\":\"admin\", \"password\":\"password\"}")
		.param("username", "admin")
		.param("password", "password")
		).andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void testAutentificacionError() throws Exception {
		// Probanbdo inicio de sesion con credenciales erroneas
		this.mockMvc.perform(post("/login")
		.param("username", "password")
		.param("password", "password")
		//  sonculta como json: .param("{\"username\":\"admin\", \"password\":\"pwderror\"}")
		).andDo(print()).andExpect(status().is4xxClientError());
	}

	

}
