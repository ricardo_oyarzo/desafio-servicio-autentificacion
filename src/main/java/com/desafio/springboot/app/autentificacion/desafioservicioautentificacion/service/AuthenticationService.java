package com.desafio.springboot.app.autentificacion.desafioservicioautentificacion.service;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import static java.util.Collections.emptyList;

public class AuthenticationService {
    static final long EXPIRATIONTIME = 864_000_00; // 1 dia en milisegundos
    static final String SIGNINGKEY = "SecretKey";
    static final String PREFIX = "Bearer";
    static final String AUTHORIZATION = "Authorization";

    // agregar token para la utentificación en el Header
    static public void addToken(HttpServletResponse res, String username) {
        String jwtToken = Jwts.builder().setSubject(username)
            .setExpiration(new Date(System.currentTimeMillis() 
                + EXPIRATIONTIME))
            .signWith(SignatureAlgorithm.HS512, SIGNINGKEY)
            .compact();
        res.addHeader(AUTHORIZATION, PREFIX + " " + jwtToken);
    res.addHeader("Access-Control-Expose-Headers", AUTHORIZATION);
    }

    // Obtener el token desde la cabecera
    static public Authentication getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(AUTHORIZATION);
        if (token != null) {
        String user = Jwts.parser()
            .setSigningKey(SIGNINGKEY)
            .parseClaimsJws(token.replace(PREFIX, ""))
            .getBody()
            .getSubject();

        if (user != null) 
            return new UsernamePasswordAuthenticationToken(user, null,
                emptyList());
        }
        return null;
    }

    private AuthenticationService() {
        
    }
    
}
