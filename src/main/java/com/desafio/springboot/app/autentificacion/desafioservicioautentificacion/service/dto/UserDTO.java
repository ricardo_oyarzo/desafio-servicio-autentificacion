package com.desafio.springboot.app.autentificacion.desafioservicioautentificacion.service.dto;

import java.time.LocalDateTime;
import javax.validation.constraints.*;

import com.desafio.springboot.app.autentificacion.desafioservicioautentificacion.model.User;


public class UserDTO {

    public static final String LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$";

    private Long id;

    @NotBlank
    @Pattern(regexp = LOGIN_REGEX)
    @Size(min = 1, max = 50)
    private String username;

    @Size(max = 50)
    private String apellidoPaterno;

    @Size(max = 50)
    private String apellidoMaterno;

    @Email
    @Size(min = 5, max = 254)
    private String email;

    @Size(max = 256)
    private String nombres;

    private String password;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getApellidoPaterno() {
        return this.apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return this.apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombres() {
        return this.nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDateTime getTokenFechaCreacion() {
        return this.tokenFechaCreacion;
    }

    public void setTokenFechaCreacion(LocalDateTime tokenFechaCreacion) {
        this.tokenFechaCreacion = tokenFechaCreacion;
    }

    private String token;
    
    private LocalDateTime tokenFechaCreacion;

    public UserDTO(User user) {


        this.id = user.getId();
        this.username = user.getUsername();
        this.apellidoPaterno = user.getApellidoPaterno();
        this.apellidoMaterno = user.getApellidoMaterno();
        this.email = user.getEmail();
    }

    public UserDTO() {
        // Empty constructor needed for Jackson.
    }
}
