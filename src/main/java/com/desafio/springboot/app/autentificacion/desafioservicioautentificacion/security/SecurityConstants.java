package com.desafio.springboot.app.autentificacion.desafioservicioautentificacion.security;


public class SecurityConstants {

    public static final String SECRET = "1234";
    public static final long EXPIRATION_TIME = 900_000; // 15 mins
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/authorization";
    public static final String ISSUER_INFO = "raomaster@gmail.com";
    


    private SecurityConstants() {

    }
  }