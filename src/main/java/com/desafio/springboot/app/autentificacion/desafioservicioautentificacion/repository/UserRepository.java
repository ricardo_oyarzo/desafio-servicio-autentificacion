package com.desafio.springboot.app.autentificacion.desafioservicioautentificacion.repository;

import java.util.Optional;

import com.desafio.springboot.app.autentificacion.desafioservicioautentificacion.model.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findOneByUsername(String username);
    User findByUsername(String username);
}
