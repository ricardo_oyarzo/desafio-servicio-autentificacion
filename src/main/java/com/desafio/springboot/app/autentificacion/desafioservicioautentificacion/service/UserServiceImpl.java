package com.desafio.springboot.app.autentificacion.desafioservicioautentificacion.service;

import com.desafio.springboot.app.autentificacion.desafioservicioautentificacion.model.User;
import com.desafio.springboot.app.autentificacion.desafioservicioautentificacion.repository.UserRepository;
import com.desafio.springboot.app.autentificacion.desafioservicioautentificacion.service.dto.UserDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import static java.util.Collections.emptyList;

import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Component
public class UserServiceImpl implements UserDetailsService {

    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> usuario = userRepository.findOneByUsername(username);
        
		if (!usuario.isPresent()) {
			throw new UsernameNotFoundException(username);
		}
		return new org.springframework.security.core.userdetails.User(usuario.get().getUsername(), usuario.get().getPassword(), emptyList());
    }

}