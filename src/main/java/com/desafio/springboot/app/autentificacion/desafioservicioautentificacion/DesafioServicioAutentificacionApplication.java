package com.desafio.springboot.app.autentificacion.desafioservicioautentificacion;

import com.desafio.springboot.app.autentificacion.desafioservicioautentificacion.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DesafioServicioAutentificacionApplication {

	@Autowired
	UserRepository userRepository;
	public static void main(String[] args) {
		SpringApplication.run(DesafioServicioAutentificacionApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(){
		return args -> {
			// Guardando informacion de prueba den la base de datos
			// userRepository.
		};
	} 

}
